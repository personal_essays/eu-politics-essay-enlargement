% Font size
\documentclass[12pt]{article}

% utf8 support
\usepackage[utf8]{inputenc}

\usepackage{hyperref}

% Referencing
\usepackage[backend=biber,style=apa]{biblatex}
\addbibresource{references.bib}

% packages for Header and Footer
\usepackage{lastpage}
\usepackage{fancyhdr}

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\title{\textbf{The EU’s enlargement policy is motivated by the ideal of spreading the EU’s values rather than by Member States’ economic interests. Discuss with reference to the case of Turkey.}}

\author{\textit{2270405d}}

 % Date, use \date{} for no date
\date{\today}

% save these variables for later use under different name
\makeatletter
\let\doctitle\@title
\let\docauthor\@author
\let\docdate\@date
\makeatother

%----------------------------------------------------------------------------------------
%	HEADER AND FOOTER SECTION
%----------------------------------------------------------------------------------------
\pagestyle{fancy}

% sets both header and footer to nothing
\fancyhf{}

% Header
% remove horizontol header bar
\renewcommand{\headrulewidth}{0pt}
% left hand side header
\lhead{\textbf{Politics of the EU Essay}}
% right hand side header
\rhead{\docauthor}


% Footer
% center of footer
\cfoot{\thepage\ of \pageref{LastPage}}

%----------------------------------------------------------------------------------------

\begin{document}

\maketitle % Print the title section

%----------------------------------------------------------------------------------------
%	ABSTRACT AND KEYWORDS
%----------------------------------------------------------------------------------------

%\renewcommand{\abstractname}{Summary} % Uncomment to change the name of the abstract to something else

%\begin{abstract}
%	abstract
%\end{abstract}

\hspace*{3.6mm}\textit{Keywords:} European Union, enlargement, Turkey, economic interest, ideal, values % Keywords

\vspace{30pt} % Vertical whitespace between the abstract and first section

%----------------------------------------------------------------------------------------
%	ESSAY BODY
%----------------------------------------------------------------------------------------

\section*{}
“Where does Europe end? Where Western Christianity ends and Islam and Orthodoxy begins.” \autocite[158]{huntington_clash_2002}. This view has been reflected in European Enlargement Policy; its failure to construct a bridge with the Muslim world through Turkish Membership is evident. This Essay will first try to discern the “European Values” as defined by the European public and the European Treaties. It will then explore how these values have been used as a pretext to veil member state’s interests, notably economic, through normative behaviour within the largely intergovernmental European Union (EU) institutions in charge of enlargement policy. It will explore this process of rhetorical action drawing from the examples of EU enlargements to Central and Eastern Europe and the Turkish candidacy.

\section*{European Values}
The 1993 European Council (EC) in Copenhagen established the Copenhagen Criteria, a set of requirements defining a state’s eligibility to become a member of the EU \autocite{european_council_presidency_1993,moravcsik_national_2003}. In a sense, these criteria define European values; a stable democracy, the rule of law, respect for human rights, protection of ethnic minorities, a market economy sustainable in the Union \autocite{european_council_presidency_1993}. An eligible state must then adopt the entirety EU’s acquis communautaire (EU law) to become a member state if the EU welcomes it \autocite{moravcsik_national_2003}. These accession criteria underline the importance of the EU’s values by setting the principle of conditionality whereby EU candidate states must implement these criteria and thus, values, in order to become member states.
Popular conceptions diverge from those expressed in the treaties by introducing an element identity, notably christian, to European values \autocite{rumford_luxembourg_2000,saz_turkophobia_2011}. Disapproval of Turkish EU membership has been found to be positively correlated with a higher proportion of Turkish population \autocite{saz_turkophobia_2011}. With an overwhelmingly Muslim population, this Turkish aversion, Turkophobia, can be qualified as a subset of a more general Islamophobia in the EU, notably amongst key member states \autocite{saz_turkophobia_2011}.
Turkey has been in rejection of some of these values, notably in the domain of freedom of expression, women’s rights, religious freedoms and the rights of minorities \autocite{rehn_europes_2006}. However, in a challenge to European democratic values, European Enlargement Policy has not reflected this view and has (albeit painfully) continued down the path of a Turkish accession to the EU, notably through a customs Union with Turkey \autocite{saz_turkophobia_2011}. As this essay will argue, EU enlargement policymaking has in many ways has not helped improve these ideological problems and has in fact instrumentalized them to put forward national interests, notably economic.

\section*{Power Asymmetry}
“The [eastern] accession took place because the dimes on offer for Eastern European states were rather insignificant for Western European states, but sufficiently significant for the Eastern European ones” \autocite[44]{zielonka_europe_2006}. There was indeed a considerable asymmetry of power between candidate states and the EU, notably for the Eastern Enlargement \autocite{henderson_black_2010,moravcsik_national_2003,zimmermann_key_2012}. It allowed the EU to imperially impose its economic, regulatory and political power on candidate states in the pursuit on its own imperial interests rather than those of candidate states \autocite{moravcsik_national_2003,rehn_europes_2006,zielonka_europe_2006,zimmermann_key_2012}.
European Enlargement Policy transforms the mere possibility of EU membership in a tool of soft power. This allows it to shape its neighbourhood through ideals of democratization and free market ideology consistent with the modernization theory of democratization, a model that might not necessarily be most appropriate for these countries \autocite{muftuler-bac_enlargement_2003,sasse_european_2008,zielonka_europe_2006}.

\section*{Depoliticized Enlargement Process}
Power asymmetry strongly depoliticizes the European Enlargement process. It allows for the EU to impose a transition to its self-favourable common market ideology on new entrants, dictated by EU negotiators and enforced by the European Court of Justice \autocite{zimmermann_key_2012}. Indeed, once the choice of adherence to the EU has been made, the adoption of EU reforms becomes a necessity for accession rather than a debate about how the process of accession should unfold \autocite{moravcsik_national_2003,zimmermann_key_2012}. Hence, the asymmetry of power involved in EU enlargement leads to a depoliticized implementation of EU technocratic policies in candidate states by establishing them as prerequisites for entry. In economic terms, EU enlargement opens new markets for current member states’ companies. Due to the Copenhagen criteria, these markets are mostly free from government intervention and in a sense ripe for the taking for foreign companies \autocite{moravcsik_national_2003,zielonka_europe_2006}.
Ironically, European enlargement policymaking is an imperial depoliticized process that imposes capitalist technocratic reform as a prerequisite for entry, justified as stabilizers of fragile democracies \autocite{zielonka_europe_2006}. In trying to “spread European values”, notably of democracy, European enlargement policy has bypassed the democratic debate. By doing so, the EU usurps its own democratic ideals championed by its Copenhagen criteria in the accession process by using the strong power asymmetry between it and candidate states \autocite{zielonka_europe_2006,zimmermann_key_2012}.

\section*{Intergovernmental Enlargement Process}
Such asymmetry of power is also mirrored within the institution. EU enlargement discussions are principally conducted through intergovernmental bargaining amongst member states \autocite{muftuler-bac_enlargement_2003}. This is notably because of the prevalence of the predominantly intergovernmental EC and the Council in the negotiations, where the commission’s supranational opinion is often acknowledged but is not biding \autocite{turhan_turkeys_2016}. The Council and the EC hold power for all the key decisions of the membership process, notably “identification of the criteria for full membership, the approval of the candidacy and the opening and closing of negotiation chapters and the granting of full membership” \autocite[464]{turhan_turkeys_2016}.
Hence, the relative power of EU member states weigh on enlargement decisions that are usually motivated by national interests veiled by rhetorical action \autocite{moravcsik_negotiating_1991,schimmelfennig_community_2001,turhan_turkeys_2016}. In accordance with realist theory, states with stronger economies, more military capacity and bigger populations see their interests favoured in intergovernmental institutions like the Council or the EC \autocite{turhan_turkeys_2016}. However, smaller member states still have a say, notably as the power asymmetry is de-emphasized by the requirement for a consensus in the EC and the possibility of a veto on enlargement matters in the Council \autocite{european_parliament_treaty_2007,turhan_turkeys_2016}.
In fact, the Turkish candidacy is a prime example of the use of vetoes in the Council to further national state interests, notably economic. Cyprus and Greece have made extensive use of this requirement for consensus which has hindered the progress of Turkish accession negotiations \autocite{muftuler-bac_turkeys_2008}. For example, Turkey’s failure to implement its plan to open Turkish harbours and airports to Cypriot traffic led to the Council’s suspension of EU-Turkey negotiations on eight chapters of the acquis based on the Cypriot opposition strengthened by its veto \autocite{muftuler-bac_turkeys_2008}.
Ultimately, the decisions regarding EU enlargement rest in the hands of the EC and the Council \autocite{avci_turkey_2013} hence, European enlargement policy is and has been driven by disparate member state interests rather than a common vision of EU enlargement \autocite{henderson_black_2010,zielonka_europe_2006}.

\section*{Rhetorical action}
This does not mean however that member states’ interests (notably economic) cannot be framed in line with European norms and values. Indeed, this crossroad between interests and values has been coined rhetorical action \autocite{schimmelfennig_community_2001}. A rhetorical action is the use of standards norms and values in a community as measures of the validity of the action within that community \autocite{aggestam_introduction_2008,schimmelfennig_community_2001}.
For example at the onset of the launch of negotiations of new chapters in the Turkish membership process in June 2013 after a favourable Council Summit, Germany vetoed the negotiations \autocite{turhan_turkeys_2016}. Officially Germany opposed violent police repression of the Gezi Park protests in Istanbul. In practice however, it was deemed an electoral calculation by the German government in view of the upcoming federal elections \autocite{turhan_turkeys_2016}. Such veiling of a member state’s interest with European norms of freedom of speech is a typically rhetorical action.

\section*{Uneven economic gains}
Furthermore, Intra-EU power discrepancies resulted in a considerably uneven spread of projected gain from the Eastern Enlargement. Indeed, pre-enlargement estimates have shown that Germany, France and the UK stood to gain two-thirds of the projected 10 Billion ECU income from the Eastern Enlargement \autocite{baldwin_costs_1997}. The widening of the Common Market is another financial benefit of enlargement, this is especially true for member states that share a border with new entrants as the smaller distances particularly favour economic exchanges between these countries \autocite{muftuler-bac_enlargement_2003,schimmelfennig_community_2001,zielonka_europe_2006}.
Neighbour states are interdependent in every domain, this applies to trade, but also to migration flows, ecological disasters, conflicts and any kind of instability \autocite{muftuler-bac_enlargement_2003,schimmelfennig_community_2001}. A concrete example is that of Germany, which was the keenest supporter of 2004 Eastern Enlargement. Germany was focused on EU accession of its geographical neighbours and key economic partners: Poland, Czechoslovakia and Hungary. The inclusion of its immediate neighbours would increase stability of its immediate surroundings and the common market would further German trade with said neighbouring countries \autocite{muftuler-bac_enlargement_2003,rumford_luxembourg_2000,sjursen_questioning_2006}.

\section*{Client-Patron relationship}
This “client/patron” relationship was also found in France’s support of the Romanian EU candidacy in an effort to gain an ally in the EU against the growing German influence \autocite{muftuler-bac_enlargement_2003,schimmelfennig_community_2001}. This also applied to Danish support of the Swedish and Finish EU applications \autocite{muftuler-bac_enlargement_2003}. Turkey had and has no “patron” within the EU, its membership was not specifically in the interest of any particular member state. The most prominent actor in its accession was the European Commission \autocite{muftuler-bac_enlargement_2003}, which, as observed above, is not central to enlargement decision-making.

\section*{Greek opposition to Turkish membership}
In fact Turkey not only lacked a patron, it had active opposition within the EU. Prior to 1999, Greece’s aversion to Turkey resulted in a longstanding Greek opposition to Ankara’s membership prospects, notably through the Greek veto of Turkish candidate status as well as European Neighbourhood Policy aid to Turkey \autocite{turhan_turkeys_2016}. Moreover, the improvement of Greek-Turk relations circa 1999 was one of the factors that allowed for the Helsinki EC to declare Turkey a candidate for EU membership without a Greek veto \autocite{muftuler-bac_enlargement_2003,turhan_turkeys_2016}. Greek entry into the Eurozone was challenged notably by Greece’s high defence spending due to its hostile relations with Turkey. More amiable Greek-Turk relations would mean higher prospects of Eurozone entry for Greece, hence the withdrawal of their veto \autocite{muftuler-bac_enlargement_2003,turhan_turkeys_2016}.

\section*{Symmetry of power with Turkey}
The Turkish candidacy differs from past EU enlargements due to Turkey’s strong position compared to past candidate states vis à vis the EU. Current demographic projection estimate Turkey will become the most powerful state in the Parliament’s voting system, weighed with member states’ population, potentially influencing decisions away from current member state’s economic interests \autocite{muftuler-bac_turkeys_2008}.
Turkish membership would mean a loss of power in the Parliament, but also 14 Billion euros in increased structural funds and Common Agricultural Policy spending due in part to the substantial Turkish agricultural sector. This figure has caused net EU budget contributors like Germany and the Netherlands to be wary of Turkish membership \autocite{muftuler-bac_turkeys_2008}.
With the destabilization of the “European neighbourhood”, Turkey has also become the gatekeeper of the European Union, essentially being paid Billions of euros by the EU to handle migration flows at its gates \autocite{turhan_turkeys_2016}. There now resides around 3.5M Syrian refugees in “Temporary Protection” in Turkey \autocite{turkish_ministry_of_interior_temporary_2018} that have been stopped by the EU’s “New Frontex”: Turkey, in exchange for large sums of cash and a strategic reopening of accession negotiations.
Now the EU finds itself in a rhetorical trap, having reopened promised accession negotiations with Turkey while simultaneously being reticent to Turkish membership due to the loss of influence in the Parliament and the economic consequences of the migration flows that would follow the opening of borders \autocite{muftuler-bac_turkeys_2008,turhan_turkeys_2016,turhan_german_2015}.

\section*{Turkish fatigue}
The laggard Turkish membership procedure has also been attributed to the EU falling short on its accession strategy for Turkey. After the marginalisation of the Turkish candidacy at the Luxembourg Council, the EU adopted a European Strategy for Turkey \autocite{rumford_luxembourg_2000}. In essence, this strategy amounted to little more than extending the EU-Turkey Customs Union through cooperation in the financial sector, far from the full pre-accession package of cooperation in communications, transport and energy given to other candidate states \autocite{rumford_luxembourg_2000}.
This has translated into an asymmetrical development of economic cooperation versus political integration in EU-Turkey relations. Today, Ankara is a major trade partner for the EU which accounts for a considerable share of Turkish exports \autocite{turhan_german_2015}. In the Turkish case, the EU has solely pushed for economic cooperation as opposed to full membership, notably due to the ebb and flows of the Turkish membership process and the numerous vetoes \autocite{turhan_turkeys_2016}. While this has benefited the member states economically, they may soon be faced with the consequence of this asymmetry. Both Turkey’s recent return to authoritarianism and the seemingly endless EU membership process have encouraged Ankara to look beyond its cooperation with the EU \autocite{turhan_german_2015}.

\section*{Conclusion}
The EU enlargement process is a highly complex mix of economy, geopolitics, values and identity. Decisions on matters of enlargement are taken in the EC and the Council and require consensus amongst member states.
This institutional architecture has shaped the Enlargement process to be largely depoliticized and intergovernmental. Thus, the enlargement process neglects the democratic values of the EU by imposing technocratic reforms to candidate states. This has allowed the EU to shape its security landscape as well as extend the reach of its common market which has brought it tangible economic benefits, but most importantly has enlarged its soft power.
There is however a junction between European values and member states’ economic interests. In fact this crossroad, dubbed rhetorical action, has been consistently implemented by member states to veil their linked political and economic interests with “European Values” as they are defined in the Copenhagen criteria, sometimes ignoring the European public’s opinion, notably with the case of Turkey.
Turkish membership is a flagship example of rhetorical action. The Turkish membership process has been the longest of any state and is still ongoing due to numerous member states interest based vetoes on the basis of the instability of the Turkish democracy. Hence, the EU is now rhetorically entrapped in its promise of Turkish membership in exchange for economic integration and border control after having consistently delayed accession talks which has resulted in Turkey’s return to authoritarianism and eastward outlook.
As this essay has shown, European values are often reconciled with member states’ interests, notably economic but only insofar as they increase the legitimacy of these interests within the normative community of the EU. Notably as the Turkish role in European “border control” has shown, states’ interests will prevail if they clash with European values.

\newpage

%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

\printbibliography

%----------------------------------------------------------------------------------------

\end{document}
